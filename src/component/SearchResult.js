import React, { Component } from "react";
import { getWord } from "../store/actions/getWordAction";
import { getSearchBuilding } from "../store/actions/getSearchAction";
import { connect } from "react-redux";
import user from "../assets/img/user.svg";
import sharp from "../assets/img/sharp.svg";
import meter from "../assets/img/speedometer.svg";
import tag from "../assets/img/tag.svg";

class SearchResult extends Component {
  state = { text: "", word: this.props.word, data: [] };


  componentDidMount(){
    this.setState({text:this.props.word})
  }

  render() {
    console.log(this.props.searchDevice);
    return (
      <div className="searchresult">
        {/* <form className="formbox d-flex justify-content-around align-items-center">
          <button className="searchbtn" onClick={this.onFormSubmit}>
            <Link to={`/SearchResult/${this.state.text}`}>جستجو</Link>
          </button>
          <input
            placeholder="شناسه کنتور مورد نظر را وارد کنید"
            type="number"
            value={this.state.text}
            onChange={this.onchangeinput}
          />
          <span>جستجو بر اساس شناسه کنتور</span>
        </form> */}

        {this.props.searchDevice.length > 0 ? (
          <h5 className="searchid mb-4">
            نتایج یافت شده با شماره کنتور {this.props.word}
          </h5>
        ) : null}

        <div className="container-fluid searchcontainer">
          <div className="row">
            {this.props.searchDevice.length > 0 ? (
              this.props.searchDevice.map((device) => {
                return (
                  <div
                    key={device.uuid}
                    className="searchresult  mt-2 border d-flex flex-column col-lg-4 bg-white"
                  >
                    <span className="d-flex">
                      <img src={user} className="icon" alt="user"/>
                      نام مالک : {device.owner}
                    </span>
                    <span>
                      <img src={sharp} className="icon" alt="sharp"/>
                      شماره شناسه : {device.zipCode}
                    </span>
                    <span>
                      <img src={meter} className="icon" alt="meter"/>
                      نوع کنتور : {device.subtype}
                    </span>
                    <span>
                      <img src={tag} className="icon" alt="tag"/>
                      برند : {device.brand}
                    </span>
{/* 
                    <div className="d-flex justify-content-center btnbox">
                      <button className="detailbtn mt-4 mb-3" type="button">
                        مشاهده جزئیات
                      </button>
                    </div> */}
                  </div>
                );
              })
            ) : (
              <div className="d-flex justify-content-center w-100">
                <h1 className="nothing">کنتوری با شناسه  {this.props.word} یافت نشد</h1>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ getWord, getSearchDevice }) => {
  return {
    word: getWord.data,
    searchDevice: getSearchDevice.data,
  };
};

export default connect(mapStateToProps, {
  getWord,
  getSearchBuilding,
})(SearchResult);
