import React from "react";
import error404 from "../assets/img/404.svg";

const NotFound = () => (
  <div className="notfound">
    <img src={error404} className="erroricon" alt="404image" />

    <h3>صفحه مورد نظر یافت نشد</h3>
  </div>
);

export default NotFound;
