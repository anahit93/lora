import React, { useState, useEffect } from "react";
// import { getSelected } from "../store/actions/getSelectedAction";
// import { getType } from "../store/actions/getTypeAction";
// import { getMasudDevices } from "../store/actions/getMasudDevices";
// import { getSinaDevices } from "../store/actions/getSinaDevices";
// import { connect } from "react-redux";
// import { withRouter } from "react-router-dom";
// import { compose } from "redux";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

const AssetsDetail = () => {
  // const [type, setType] = useState("");

  // const dispatch = useDispatch();
  useEffect(() => {}, []);

  const typename = useSelector((state) => state.getType.data);
  console.log(typename);

  const selected = useSelector((state) => state.getSelected.data);
  console.log(selected);
  let history = useHistory();

  function goBack() {
    history.goBack();
  }

  return (
    <div className="detailbox">
      <div className="d-flex justify-content-center mb-3">
        <h4>جزئیات دستگاه</h4>
      </div>
      <p>نام برج : {selected.name} </p>
      <p>آدرس : {selected.address}</p>
      <p>شهر : {selected.city}</p>
      <p>کشور : {selected.country}</p>
      <p>تلفن : {selected.phone}</p>
      <p>استان : {selected.state}</p>
      <p>نوع سکونت: {selected.type}</p>
      <p>نوع مالکیت: {selected.subtype}</p>
      <div className="d-flex justify-content-center">
        {/* <p>{this.props.masudCount}</p> */}
        {/* <p>{selected.length}: {typename}  تعداد کنتور</p> */}
      </div>
      <button className="backbtn" onClick={goBack}>
        بازگشت
      </button>
    </div>
  );
};

export default AssetsDetail;

// class AssetsDetail extends Component {
//   state = {
//     select: [],
//     type: "",
//     city: "",
//     address: "",
//     country: "",
//     name: "",
//     phone: "",
//     state: "",
//     type2: "",
//     subtype: "",
//   };

//   componentDidMount() {
//     this.props.getSelected();
//     console.log(this.props.selected);
//     this.setState({ city: this.props.selected.city });
//     this.setState({ address: this.props.selected.address });
//     this.setState({ country: this.props.selected.country });
//     this.setState({ name: this.props.selected.name });
//     this.setState({ phone: this.props.selected.phone });
//     this.setState({ state: this.props.selected.state });
//     this.setState({ type2: this.props.selected.type });
//     this.setState({ subtype: this.props.selected.subtype });
//     this.setState({ type: this.props.type });
//   }

//   goBack=()=>{
//     this.props.history.goBack()
// }

//   render() {
//     console.log(this.state.city);
//     return (
//       <div>
//         <div className="detailbox">
//           <div className="d-flex justify-content-center mb-3">
//             <h4>جزئیات دستگاه</h4>
//           </div>
//           <p>نام برج : {this.state.name} </p>
//           <p>آدرس : {this.state.address}</p>
//           <p>شهر : {this.state.city}</p>
//           <p>کشور : {this.state.country}</p>
//           <p>تلفن : {this.state.phone}</p>
//           <p>استان : {this.state.state}</p>
//           <p>نوع سکونت: {this.state.type2}</p>
//           <p>نوع مالکیت: {this.state.subtype}</p>
//           <div className="d-flex justify-content-center">
//             <p>{this.props.masudCount}</p>
//             <p>: {this.state.type} تعداد </p>
//           </div>
//             <button className="backbtn" onClick={this.goBack}>
//               بازگشت
//             </button>
//         </div>
//       </div>
//     );
//   }
// }

// const mapStateToProps = ({
//   getSelected,
//   getType,
//   getMasudDevicesReducer,
//   getSinaDevicesReducer,
// }) => {
//   return {
//     selected: getSelected.data,
//     type: getType.data,
//     masudCount: getMasudDevicesReducer.data,
//     sinaCount: getSinaDevicesReducer.data,
//   };
// };

// export default compose(
//   withRouter,
//   connect(mapStateToProps, {
//     getSelected,
//     getType,
//     getMasudDevices,
//     getSinaDevices,
//   })
// )(AssetsDetail);
