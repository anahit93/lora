import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
} from "react-router-dom";
import MapComponent from "./MapComponent";
import Gozaresh from "./Gozaresh";
import Tanzimat from "./Tanzimat";
import AssetsDetail from "./AssetsDetail";
import DevicesDetail from "./DevicesDetail";
import SearchResult from "./SearchResult";
import NotFound from "./NotFound";
import Taghvim from './Taghvim';
import Bill from './Bill';

class Navbar extends Component {
  state = {};

  render() {
    return (
      <div>
        <Router>
          <div className="d-flex flex-column">
            <div>
              <header className="header">
                <label className="menu-icon" htmlFor="menu-btn">
                  <span className="navicon"></span>
                </label>
                {/* <a href="" class="logo">CSS Nav</a> */}
                <input className="menu-btn" type="checkbox" id="menu-btn" />
                {/* <div className="menuicon"> <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>
</div> */}
                <ul className="menu">
                  <li>
                    <NavLink
                      exact
                      to="/"
                      className="main-nav"
                      activeClassName="main-nav-active"
                    >
                      نمایش لحظه ای
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to="/Gozaresh"
                      className="main-nav"
                      activeClassName="main-nav-active"
                    >
                      گزارشات
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to="/Tanzimat"
                      className="main-nav"
                      activeClassName="main-nav-active"
                    >
                      تنظیمات
                    </NavLink>
                  </li>
                </ul>
              </header>
            </div>
            <div className="pt-5 switchclass">
              <Switch>


              <Route exact path="/gozaresh/Taghvim/bill/:id">
                  <Bill/>
                </Route>

              <Route exact path="/gozaresh/Taghvim/:id">
                  <Taghvim/>
                </Route>


                <Route exact path="/gozaresh/SearchResult/:id">
                  <SearchResult />
                </Route>
                

                <Route path={`/AssetsDetail/:id`}>
                  <AssetsDetail />
                </Route>

                <Route path={`/DevicesDetail/:id`}>
                  <DevicesDetail />
                </Route>

                <Route path="/Tanzimat">
                  <Tanzimat />
                </Route>

                <Route path="/Gozaresh">
                  <Gozaresh />
                </Route>

                <Route exact path="/">
                  <MapComponent />
                </Route>

                <Route path="*">
                  <NotFound />
                </Route>
              </Switch>
            </div>
          </div>
        </Router>
      </div>
    );
  }
}

export default Navbar;
