import React, { Component } from 'react';
import { connect } from "react-redux";
import {getSelectedD} from '../store/actions/getSelectedDeviceAction';
import { withRouter } from 'react-router-dom';
import {compose} from 'redux';

var selectedDevice=[];

class DevicesDetail extends Component {
    state = { test:[]}

    componentDidMount(){
        this.props.getSelectedD();
        console.log(this.props.selectedDevice);
        selectedDevice.push(this.props.selectedDevice);
        console.log(selectedDevice);
        this.setState({test:selectedDevice})  
    }

    render() { 
      console.log(this.state.test)
        return ( <div className="DeviceDetail">
          {this.state.test.map(device=>{
            return <div key={device.uuid} className="d-flex flex-column deviceboxdetail">
            <span> {device.name} : نام </span>
            <span>برند : {device.brand}</span>
            <span>توضیحات : {device.description}  </span>
            <span> {device.factoryID} : شماره شناسایی کارخانه </span>
            <span> {device.model} : مدل </span>
            <span>صاحب کنتور : {device.owner} </span>
            <span>نوع : {device.type}  </span>
            <span>زیرگروه : {device.subtype} </span>
            
            <div className="d-flex justify-content-center mt-3">
                <button className="backbtn" onClick={this.props.history.goBack}>بازگشت</button>
              </div>
            </div>
          })}
        </div> );
    }
}
 
const mapStateToProps = ({ getSelectedD }) => {
    return {
      selectedDevice:getSelectedD.data
    };
  };

  export default compose(withRouter,connect(mapStateToProps,{
    getSelectedD
  }))(DevicesDetail)
  
