import React, { useState,useEffect } from "react";
// import { getTime } from "../store/actions/getbillTimeAction";
import { getbill } from "../store/actions/getbillAction";
// import { getSelect } from "../store/actions/getSelectedItemAction";
import moment from "jalali-moment";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
// import { useHistory } from "react-router-dom";
import waterimage from '../assets/img/waterimg.jpg'
import gasimage from '../assets/img/gasimg.jpg'
import elecimage from '../assets/img/electricityimg.jpg'


const Bill = () => {


  const [imagesrc, setImagesrc] = useState('');
  const [begin, setBegin] = useState('');
  const [end, setEnd] = useState('');
  const [amount, setAmount] = useState('');
  const [unixstart, setUnixstart] = useState('');
  const [unixend, setUnixend] = useState('');
  const [uuidselect, setUuidselect] = useState('');

  // let history = useHistory();
  const dispatch = useDispatch();

  
  useEffect(() => {  


   


    if (selectItem.type === "water") {
      setImagesrc(waterimage)
    } else if (selectItem.type === "gas") {
      setImagesrc(gasimage)
    } else {
      setImagesrc(elecimage)
    }



              console.log(time.beginTime)
    var testtimebegin = moment
    .from(time.beginTime);
    var testtimeend = moment
    .from(time.endTime);


          if (time.endTime) {
    
            setBegin( testtimebegin._i.year +"-" +testtimebegin._i.month +"-" +testtimebegin._i.day );
      setEnd( testtimeend._i.year +"-" +testtimeend._i.month +"-" +testtimeend._i.day  );


      var begindate = moment
        .from(time.beginTime)
        .format("YYYY-M-D HH:mm:ss");
      console.log(begindate);

      var begindatewithouttime = begindate.split(" ")[0];
      console.log(begindatewithouttime);
      console.log(begindatewithouttime);
      var begindateUnix = (new Date(begindate).getTime() / 1000) * -1;
      console.log(begindateUnix);
      setUnixstart(begindateUnix)

      var enddate = moment
        .from(time.endTime)
        .format("YYYY-M-D HH:mm:ss");
      console.log(enddate);
      var enddatewithouttime = enddate.split(" ")[0];
      console.log(enddatewithouttime);
            
      var enddateUnix = (new Date(enddate).getTime() / 1000) * -1;
      console.log(enddateUnix);
      setUnixend(enddateUnix)
    

 
    }
 


  setUuidselect(selectItem.uuid);
    setAmount(bill.payableAmount );
   console.log(uuidselect)
   console.log(selectItem.uuid)

    dispatch(getbill({id:selectItem.uuid, beginTime:begindateUnix,endTime:enddateUnix}));

  }, [getbill]); 

  const bill = useSelector((state) => state.getbill.data);
  console.log(bill);
  console.log(bill.payableAmount);
  

  const time = useSelector((state) => state.getTime.data);
  console.log(time);
  console.log(begin)
  console.log(end)

  const selectItem = useSelector((state) => state.getSelect.data);
  console.log(selectItem);
  console.log(selectItem.uuid)



let classes;
if (selectItem.type === "water") {
  classes="ab"
}
 else if (selectItem.type === "gas") {
  classes="gaz"
} else {
  classes="bargh"
}


  return (
           <div className="billsection">
          <div className={classes}>
            <div className="logoframe">
              <img
                src={imagesrc}
                className="logoimage"
                alt="logoimage"
              />
            </div>
          
  
            
            <span> {begin} : تاریخ شروع </span>
            <span> {end} : تاریخ پایان</span>
            <span> {bill.payableAmount} : قابل پرداخت</span>
            <span> آدرس : {bill.address} </span>
            <span> صاحب کنتور : {bill.owner} </span>
            <span> {bill.payableAmount} : مبلغ قابل پرداخت</span>
            <span> {bill.readingMethod} : نحوه قرائت</span>
            <span> {bill.readingOperator} : اپراتور قرائت</span>
            <span> {bill.remainingRials} : مبلغ بدهکاری</span>
            <span> {bill.zipCode} : کدپستی</span>
            <span> {bill.type} : نوع قبض</span>
          
        
          </div>
        </div> );
}
 
export default Bill;
