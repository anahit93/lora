import {GET_BUILDINGS_Assets} from '../../Type/Types'

const INITIAL_STATE ={
    data:[]
}


export default function getBuildingassets( state= INITIAL_STATE, action){
    switch(action.type){
        case GET_BUILDINGS_Assets:
            // console.log(action.payload.data.data)
            return {
                data:action.payload.data.data
    };
   
        default:
            return state;
    }
    
    
}
