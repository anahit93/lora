import { combineReducers } from 'redux';

import {persistReducer} from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import getAllBuildings from './mapReducer';
import getBuildingassets from './buildingAssetsReducer';
import getSinaDevicesReducer from './getSinaDevicesReducer';
import getMasudDevicesReducer from './getMasudDevicesReducer';
import getSelected from './SelectedReducer';
import getType from './getTypeMeterReducer';
import getAssetReducer from './getAssetReducer';
import getbuildingdevices from './buildingDevicesReducer';
import getSelectedD from './SelectedDeviceReducer';
import getSearchDevice from './getSearchReducer';
import getWord from './getWordReducer';
import getSelect from './SelectedItemReducer';
import getbill from './getbillReducer';
import getTime from './getBillTimeReducer';




export default combineReducers({

    getAllBuildings :persistReducer({key: 'getKeyBuilding',storage,whiteList:['data']},getAllBuildings),
    getBuildingassets :persistReducer({key: 'getKeyBuildingassets',storage,whiteList:['data']},getBuildingassets),
    getSinaDevicesReducer :persistReducer({key: 'getSinaDevicesReducer',storage,whiteList:['data']},getSinaDevicesReducer),
    getMasudDevicesReducer :persistReducer({key: 'getMasudDevicesReducer',storage,whiteList:['data']},getMasudDevicesReducer),
    getSelected :persistReducer({key: 'getSelected',storage,whiteList:['data']},getSelected),
    getType :persistReducer({key: 'getType',storage,whiteList:['data']},getType),
    getAssetReducer :persistReducer({key: 'getAssetReducer',storage,whiteList:['data']},getAssetReducer),
    getbuildingdevices :persistReducer({key: 'getKeybuildingdevices',storage,whiteList:['data']},getbuildingdevices),
    getSelectedD :persistReducer({key: 'getKeyselectedd',storage,whiteList:['data']},getSelectedD),
    getSearchDevice :persistReducer({key: 'getKeySearchDevice',storage,whiteList:['data']},getSearchDevice),
    getWord :persistReducer({key: 'getKeyWord',storage,whiteList:['data']},getWord),
    getSelect :persistReducer({key: 'getKeySelect',storage,whiteList:['data']},getSelect),
    getbill :persistReducer({key: 'getKeybill',storage,whiteList:['data']},getbill),
    getTime :persistReducer({key: 'getKeyTime',storage,whiteList:['data']},getTime),
})