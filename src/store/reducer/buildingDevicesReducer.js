import {GET_BUILDINGS_Devices} from '../../Type/Types'

const INITIAL_STATE ={
    data:[]
}


export default function getBuildingdevices( state= INITIAL_STATE, action){
    switch(action.type){
        case GET_BUILDINGS_Devices:
            console.log(action.payload.data.data)
            return {
                data:action.payload.data.data
    };
   
        default:
            return state;
    }
    
    
}
