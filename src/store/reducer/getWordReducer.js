import {GET_SEARCH_WORD} from '../../Type/Types'

const INITIAL_STATE ={
    data:''
}


export default function getWord( state= INITIAL_STATE, action){
    switch(action.type){
        case GET_SEARCH_WORD:
            // console.log(action.payload.data.data[0].uuid)
            return {
                data:action.payload
    };
   
        default:
            return state;
    }
    
    
}
