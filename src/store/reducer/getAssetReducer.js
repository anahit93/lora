import {GET_ASSET_ACTION} from '../../Type/Types'

const INITIAL_STATE ={
    data:[]
}


export default function getAssetReducer( state= INITIAL_STATE, action){
    switch(action.type){
        case GET_ASSET_ACTION:
            // console.log(action.payload.data.data.length)
            return {
                
                data:action.payload.data.data.length
    };
   
        default:
            return state;
    }
    
    
}
