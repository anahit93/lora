import {GET_SEARCH_ACTION} from '../../Type/Types'

const INITIAL_STATE ={
    data:[]
}


export default function getSearchDevice( state= INITIAL_STATE, action){
    switch(action.type){
        case GET_SEARCH_ACTION:
            console.log(action.payload.data.data)
            return {
                data:action.payload.data.data
    };
   
        default:
            return state;
    }
    
    
}
