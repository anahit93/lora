import {GET_SELECTED_DEVICE} from '../../Type/Types'

const INITIAL_STATE ={
    data:''
}


export default function getSelectedD( state= INITIAL_STATE, action){
    switch(action.type){
        case GET_SELECTED_DEVICE:
            return {
                data:action.payload
    };
   
        default:
            return state;
    }
    
    
}
