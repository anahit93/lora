import {GET_BILL_DEVICE} from '../../Type/Types'

const INITIAL_STATE ={
    data:''
}


export default function getbill( state= INITIAL_STATE, action){
    switch(action.type){
        case GET_BILL_DEVICE:
            console.log(action.payload.data)
            return {
                data:action.payload.data
    };
   
        default:
            return state;
    }
    
    
}
