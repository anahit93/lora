import {GET_METER_TYPE} from '../../Type/Types'

const INITIAL_STATE ={
    data:''
}


export default function getType( state= INITIAL_STATE, action){
    switch(action.type){
        case GET_METER_TYPE:
            // console.log(action.payload.data.data[0].uuid)
            return {
                data:action.payload
    };
   
        default:
            return state;
    }
    
    
}
