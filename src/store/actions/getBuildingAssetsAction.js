import { GET_BUILDINGS_Assets } from "../../Type/Types";
import axios from "axios";

const getbuildingassetsapi = (data) => ({
  type: GET_BUILDINGS_Assets,
  payload: data,
});

export const getbuildingassets = (uuid) => {
  return (dispatch) => {
    axios
      .get(`https://api.lora.atrovan.com/api/assets/building/${uuid}?limit=100`)
      
      .then((data) => {
        // console.log(data);
        dispatch(getbuildingassetsapi(data));
      })
      .catch((e) => {});
  };
};
