import { GET_METER_TYPE } from "../../Type/Types";


const getMeterType = (data) => ({
  type: GET_METER_TYPE,
  payload: data,
});

export const getType = (type) => {
  return (dispatch) => {
  //  console.log(type)
        dispatch(getMeterType(type));
    
  };
};