import { GET_SEARCH_WORD } from "../../Type/Types";


const getSearchWord = (data) => ({
  type: GET_SEARCH_WORD,
  payload: data,
});

export const getWord = (word) => {
  return (dispatch) => {
//    console.log(select)
        dispatch(getSearchWord(word));
    
  };
};