import { GET_Asset_Devices } from "../../Type/Types";
import axios from "axios";

const getSinaDevicesReducer = (data) => ({
  type: GET_Asset_Devices,
  payload: data,
});

export const getSinaDevices = ({id,type}) => {
  return (dispatch) => {
    axios
      .get(`https://api.lora.atrovan.com/api/devices/asset/${id}?limit=100&type=${type}`)
      
      .then((data) => {
        // console.log(data);
        dispatch(getSinaDevicesReducer(data));
      })
      .catch((e) => {});
  };
};


// export const getSinaDevices2 = ({id,type}) => {
//   return (dispatch) => {
//     axios
//       .get(`https://api.lora.atrovan.com/api/devices/asset/${id}?limit=100&type=${type}`)
      
//       .then((data) => {
//         // console.log(data);
//         dispatch(getSinaDevicesReducer(data));
//       })
//       .catch((e) => {});
//   };
// };
