import { GET_SEARCH_ACTION } from "../../Type/Types";
import axios from "axios";

const getsearchapi = (data) => ({
  type: GET_SEARCH_ACTION,
  payload: data,
});

export const getSearchBuilding = (textsearch) => {
  return (dispatch) => {
    axios
      .get(
        `https://api.lora.atrovan.com/api/devices/building/b15406c0-d01c-11ea-91d5-53a3a6b91e65?limit=100&textSearch=${textsearch}`
      )

      .then((data) => {
        // console.log(data);
        dispatch(getsearchapi(data));
      })
      .catch((e) => {});
  };
};
