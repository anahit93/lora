import { GET_ASSET_ACTION } from "../../Type/Types";
import axios from "axios";

const getAssetReducer = (data) => ({
  type: GET_ASSET_ACTION,
  payload: data,
});

export const getAsset = ({id,type}) => {
  return (dispatch) => {
    axios
      .get(`https://api.lora.atrovan.com/api/devices/asset/${id}?limit=100&type=${type}`)
      
      .then((data) => {
        // console.log(data);
        dispatch(getAssetReducer(data));
      })
      .catch((e) => {});
  };
};
