import { GET_SELECTED_ITEM } from "../../Type/Types";


const getSelectedItem = (data) => ({
  type: GET_SELECTED_ITEM,
  payload: data,
});

export const getSelect = (select) => {
  return (dispatch) => {
//    console.log(select)
        dispatch(getSelectedItem(select));
    
  };
};