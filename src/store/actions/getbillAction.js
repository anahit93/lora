import { GET_BILL_DEVICE } from "../../Type/Types";
import axios from "axios";

const getbillapi = (data) => ({
  type: GET_BILL_DEVICE,
  payload: data,
});

export const getbill = ({id,beginTime,endTime}) => {
  return (dispatch) => {
    axios
      .get(`https://api.lora.atrovan.com/api/device/${id}/bill?beginTime=${beginTime}&endTime=${endTime}`)
      
      .then((data) => {
        console.log(data);
        dispatch(getbillapi(data));
      })
      .catch((e) => {});
  };
};