import { GET_Asset_Devices2 } from "../../Type/Types";
import axios from "axios";

const getMasudDevicesReducer = (data) => ({
  type: GET_Asset_Devices2,
  payload: data,
});

export const getMasudDevices = ({id, type}) => {
  return (dispatch) => {
    axios
      .get(`https://api.lora.atrovan.com/api/devices/asset/${id}?limit=100&type=${type}`)
      
      .then((data) => {
        dispatch(getMasudDevicesReducer(data));
      })
      .catch((e) => {});
  };
};
