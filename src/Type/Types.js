export const GET_ALL_BUILDINGS = "get_all_buildings"

export const GET_BUILDINGS_Assets = "get_buildings_assets"

export const GET_Asset_Devices = "get_asset_devices"

export const GET_Asset_Devices2 = "get_asset_devices2"

export const GET_SELECTED_ASSET = "get_selected_asset"

export const GET_METER_TYPE = "get_meter_type"

export const GET_ASSET_ACTION = "get_asset_action"

export const GET_BUILDINGS_Devices  = "get_building_devices"

export const GET_SELECTED_DEVICE  = "get_selected_device"

export const GET_SEARCH_ACTION  = "get_search_action"

export const GET_SEARCH_WORD  = "get_search_word"

export const GET_SELECTED_ITEM  = "get_selected_item"

export const GET_BILL_DEVICE  = "get_bill_device"

export const GET_BILL_TIME  = "get_bill_time"




